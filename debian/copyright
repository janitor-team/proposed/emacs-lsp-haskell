Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lsp-haskell
Source: https://github.com/emacs-lsp/lsp-haskell

Files: *
Copyright: 2018-2020 Alan Zimmerman <alan.zimm@gmail.com>
           2017 Vibhav Pant <vibhavp@gmail.com>
License: GPL-3+

Files: images/logo.png
Copyright: 2009 Darrin Thompson, Jeff Wheeler
Comment: original source of the logo https://wiki.haskell.org/Thompson-Wheeler_logo
 The copyright of all wiki content https://wiki.haskell.org/HaskellWiki:Copyrights
License: simple-permissive
 Permission is hereby granted, free of charge, to any person obtaining this
 work (the "Work"), to deal in the Work without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Work, and to permit persons to whom the
 Work is furnished to do so.
 .
 THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS IN THE
 WORK.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in `/usr/share/common-licenses/GPL-3'
